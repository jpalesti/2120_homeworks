/*
*
*  @author Joe Palestina
*  @version 1.10
* In this class a Complex Number (a+bi) can be added, subtracted, multiplied, or divided with other complex numbers.
* There is an overwritten toString method to properly format in a+bi
*
*
*/


public class ComplexNumber{
         

     /**
     *  Instance Variables representing real parts of the complex number
     **/
     private float a;
     private float b;

    /**
     * Constructs a new Complex Number
     * 
     * @param _a is the real number
     * @param _b is the number applied to i
     *
     */
     public ComplexNumber(float _a, float _b){
          this.a = _a;
          this.b = _b;
     }
    

    /**
     * Returns the real number value of a
     * @return value of the real number a
     *
     */    
     public float getA(){
          return this.a;
     }

     /**
     * Returns the value of b (a+bi)
     * @return value of b
     *
     */ 
     public float getB(){
          return this.b;
     }

    /**
     * This method adds both a together and b together
     * 
     * @param Complex Number object to be added to this Complex number
     * @return two floats that make up the a nd b of a+bi
     */
     public ComplexNumber add(ComplexNumber otherNumber){
          ComplexNumber newComplex;
          float newA = a + otherNumber.getA();
          float newB = b + otherNumber.getB();
          newComplex = new ComplexNumber(newA, newB);
          return newComplex;
     }

    /**
     * This method subtracts the two complex numbers
     * 
     * @param Complex Number object to be subtracted from this Complex number
     * @return two floats that make up the a nd b of a+bi
     */
    public ComplexNumber subtract(ComplexNumber otherNumber){
        ComplexNumber newComplex;
        float newA = a - otherNumber.a;
        float newB = b - otherNumber.b;
        newComplex = new ComplexNumber(newA, newB);
        return newComplex;
    }// end subtract

    /**
     * This method multiplies the two complex numbers
     * 
     * @param Complex Number object to be multiplied to this Complex number
     * @return two floats that make up the a nd b of a+bi
     */
    public ComplexNumber multiply(ComplexNumber otherNumber){
        ComplexNumber newComplex;
        float newA = (a * otherNumber.a)-(b * otherNumber.b);
        float newB = (b * otherNumber.a)+(a * otherNumber.b);
        newComplex = new ComplexNumber(newA, newB);
        return newComplex;
    }// end multiply
    
    /**
     * This method divides the two complex numbers
     * 
     * @param Complex Number object to be divided into this Complex number
     * @return two floats that make up the a nd b of a+bi
     */
    public ComplexNumber divide(ComplexNumber otherNumber){
        ComplexNumber newComplex;
        float newA = ((a * otherNumber.a)+(b * otherNumber.b)) / ((otherNumber.a * otherNumber.a)+(otherNumber.b * otherNumber.b));
        float newB = ((b * otherNumber.a)-(a * otherNumber.b)) / ((otherNumber.a * otherNumber.a)+(otherNumber.b * otherNumber.b));
        newComplex = new ComplexNumber(newA, newB);
        return newComplex;
    }// end divide

    /**
     * This method is to format both 'a' and 'b'properly in the format 'a+bi'
     * @return string that displays in 'a+bi' format
     */
    public String toString(){
        String string = this.a +" + "+ this.b+"i";
        return string;
    }//end toString




     
}






