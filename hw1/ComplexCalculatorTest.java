/*
*
* @author Joe Palestina
* @version 1.3
* 
*This class provides JUnit Testing for ComplexNumber.java
*
*
*/

import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;


public class ComplexCalculatorTest{
	
	 /**
     *  Instance Variables
     **/
	private ComplexNumber complexNumber1;
	private ComplexNumber complexNumber2;
	private ComplexNumber complexNumber3;
	private ComplexNumber complexNumber4;


	/* 
	*This method Sets up four Complex Numbers to test
	*
	*/
	@Before
	public void setUp(){
		complexNumber1 = new ComplexNumber(1 , 2);
		complexNumber2 = new ComplexNumber(3 , 4);
		complexNumber3 = new ComplexNumber(29 , 5);
		complexNumber4 = new ComplexNumber(0 , 17);
	}

	/* 
	*This method tests addition
	* When assertEquals with float type, must have delta value to allow for variance
	*/
	@Test
	public void testAdd(){
		ComplexNumber answer;
		answer = complexNumber1.add(complexNumber2);
		assertEquals(answer.getA(), 4, 0.01);
		assertEquals(answer.getB(), 6, 0.01);
		
		answer= complexNumber2.add(complexNumber3);
		assertEquals(answer.getA(), 32, 0.01);
		assertEquals(answer.getB(), 9, 0.01);
		
		answer = complexNumber4.add(complexNumber3);
		assertEquals(answer.getA(), 29, 0.01);
		assertEquals(answer.getB(), 22, 0.01);
		
		answer = complexNumber4.add(complexNumber1);
		assertEquals(answer.getA(), 1, 0.01);
		assertEquals(answer.getB(), 19, 0.01);
		
	}
	
	/* 
	*This method tests subtraction
	*
	*/
	@Test
	public void testSubtract(){
		ComplexNumber answer;
		answer = complexNumber1.subtract(complexNumber2);
		assertEquals(answer.getA(), -2, 0.01);
		assertEquals(answer.getB(), -2, 0.01);
		
		answer= complexNumber2.subtract(complexNumber3);
		assertEquals(answer.getA(), -26, 0.01);
		assertEquals(answer.getB(), -1, 0.01);
		
		answer = complexNumber4.subtract(complexNumber3);
		assertEquals(answer.getA(), -29, 0.01);
		assertEquals(answer.getB(), 12, 0.01);
		
		answer = complexNumber4.subtract(complexNumber1);
		assertEquals(answer.getA(), -1, 0.01);
		assertEquals(answer.getB(), 15, 0.01);
	}

	/* 
	*This method tests subtraction
	*
	*/
	@Test
	public void testMultiply(){
		ComplexNumber answer;
		answer = complexNumber1.multiply(complexNumber2);
		assertEquals(answer.getA(), -5, 0.01);
		assertEquals(answer.getB(), 10, 0.01);
		
		answer= complexNumber2.multiply(complexNumber3);
		assertEquals(answer.getA(), 67, 0.01);
		assertEquals(answer.getB(), 131, 0.01);
		
		answer = complexNumber4.multiply(complexNumber3);
		assertEquals(answer.getA(), -85, 0.01);
		assertEquals(answer.getB(), 493, 0.01);
		
		answer = complexNumber4.multiply(complexNumber1);
		assertEquals(answer.getA(), -34, 0.01);
		assertEquals(answer.getB(), 17, 0.01);
	}
	
	/* 
	*This method tests division
	*
	*/
	@Test
	public void testDivide(){
		ComplexNumber answer;
		answer = complexNumber1.divide(complexNumber2);
		assertEquals(answer.getA(), 0.44, 0.01);
		assertEquals(answer.getB(), 0.08, 0.01);

		answer= complexNumber2.divide(complexNumber3);
		assertEquals(answer.getA(), 0.12, 0.01);
		assertEquals(answer.getB(), 0.12, 0.01);
		
		answer = complexNumber4.divide(complexNumber3);
		assertEquals(answer.getA(), 0.1, 0.01);
		assertEquals(answer.getB(), 0.57, 0.01);
		
		answer = complexNumber4.divide(complexNumber1);
		assertEquals(answer.getA(), 6.8, 0.01);
		assertEquals(answer.getB(), 3.4, 0.01);
	}
}