/*
* Recursion Class w/ JUnit Testing
*
* @author   Joe Palestina
* @version  23 September 2016
*
* This class has methods that compare Strings and determines alphabetical order.
* This class also has JUnit Test for those classes.
*
*/
import static org.junit.Assert.*;
import org.junit.Test;
import org.junit.Before;
import java.util.ArrayList;
import java.util.List;
import java.util.Arrays;

public class Recursion {
    
    public static void main(String[] args){
       //System.out.println(compareThese("blue","Xylophone"));
       //ArrayList<String> computerBlue = new ArrayList<String>(Arrays.asList("banana", "blue", "xylophone","waffle", "applesauce"));
       //System.out.println(findMinimum(computerBlue));
    }
    
    /*This method comapres 2 strings to check alphabetical order 
    * If string1 comes before string 2 alphabetically,  expected outcome is negative distance away from opposing letters
    * If both are the same expected is 0
    * If string2 comes before string one alphabetically, expected outcome is positive distance away from opposing letters
    * @param 1st string to compare
    * @param 2nd string to compare
    * @return positive or negative int. 0 if no String is entered
    */
    public static int compareThese(String s1, String s2){
        s1 = s1.toLowerCase();
        s2 = s2.toLowerCase();
        // return as indistinguishable
        if (s1.length() == 0 && s2.length() == 0){
            return 0;
        }
        else if (s1.length() == 0){
            return -1;
        }
        else if (s2.length() == 0){
            return 1;
        }
        else if (s1.charAt(0) != s2.charAt(0)){
            
            return s1.charAt(0) - s2.charAt(0); 
           
        }
        else{
            return compareThese(s1.substring(1), s2.substring(1));
            
        }
        
    }

    /*This method takes an ArrayList as parameter, finds a Min, then sends it to an the next method (of the same name) with another parameter
    * @param ArrayList<String> Strings in a nonspecific order
    */
    private static String findMinimum(ArrayList<String> list){
        return findMinimum(list.subList(1,list.size()),list.get(0));
    }
    
    /*This method takes an ArrayList as parameter, as well as a selected min from the previous method. 
    *It then recursively goes through the List to find the alphabetical first and returns that String
    * @param ArrayList<String> Strings in a nonspecific order
    * @param String currentMin is one of the items of the array list to compare to the others.
    * @return currentMin returns alphabetical first String
    */
    private static String findMinimum(List<String> list, String currentMin){
        if (list.size() == 0){
            return currentMin;
        }
        else {
            String minOfTail = findMinimum(list.subList(1,list.size()),list.get(0));
            if (compareThese(minOfTail,currentMin) < 0){
                System.out.println("minOfTail: " + minOfTail +" currentMin: " + currentMin );
                return minOfTail;
            }
            else {
                return currentMin;
            }//end else returning currentMin
        }//end else if needed to continue
    }//end findMinimum(<List>, String)
    
    /*˚
    *Instance Variables for JUnit Testing
    */
    private String word0 = "BuBbLe";
    private String word1 = "bubble";
    private String word2 = "Bubbles";
    private String word3 = "xylophone";
    private String word4 = "blue";
    private String word5 = "applesauce";
    private String word6 = "Penelope";
    private String word7 = "panCakes";
    private String word8 = "waffles";
    private String word9 = "blouses";
    private String word10 = "blouses";
  
    private ArrayList<String> array1 = new ArrayList<String>(Arrays.asList( word1, word2, word3, word4, word5, word6, word7, word8, word9));
    private ArrayList<String> array2 = new ArrayList<String>(Arrays.asList( word9, word6, word3, word4, word2, word5, word7, word1, word8));
    private ArrayList<String> array3 = new ArrayList<String>(Arrays.asList( word5, word6, word7, word8, word9));
    private ArrayList<String> array4 = new ArrayList<String>(Arrays.asList( word1, word2, word3, word4, word0, word9 ));
    private ArrayList<String> array5 = new ArrayList<String>(Arrays.asList( word8, word7, word3, word6));
    private ArrayList<String> array6 = new ArrayList<String>(Arrays.asList( word0, word2, word4, word6, word8));

    
    /*
    *Testing the compareThese method
    * this tests the two strings compared to one another. 
    * If string1 comes before string 2 alphabetically,  expected outcome is negative distance away from opposing letters
    * If both are the same expected is 0
    * If string2 comes before string one alphabetically, expected outcome is positive distance away from opposing letters
    */
    @Test
    public void testCompareThese(){
        assertEquals(compareThese(word0, word1), 0);
        assertEquals(compareThese(word2, word0), 1);
        assertEquals(compareThese(word3, word4), 22);
        assertEquals(compareThese(word8, word6), 7);
        assertEquals(compareThese(word0, word9), 9);
        assertEquals(compareThese(word9, word10), 0);
        assertEquals(compareThese(word6, word8), -7);
        assertEquals(compareThese(word5, word7), -15);
    }//end testCompareThese
    
    /*
    *
    *This method tests for the alphabettically first word in an array list.
    *
    *
    */
    @Test
    public void testFindMinimum(){
        assertEquals(findMinimum(array1), "applesauce");
        assertEquals(findMinimum(array2), "applesauce");
        assertEquals(findMinimum(array3), "applesauce");
        assertEquals(findMinimum(array4), "blouses");
        assertEquals(findMinimum(array5), "panCakes");
        assertEquals(findMinimum(array6), "blue");
    }//end testFindMinimum
    
}//end class



    

